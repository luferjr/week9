**Readme Instructions for CSE2ICE - Week7**

Create an example Angular application called ExampleApp to demonstrate multiple components redering.

## Create Angular Application

All angular requirements need to be installed: Node, NPM, and Angular client

1. Create App called ExampleApp
$ ng new ExampleApp (accept all default options)

2. Validate that ExampleApp was created properly
$ cd ExampleApp
$ ng serve -o

The new Angular application will open in the browser (http://localhost:4200)

## Create LeagueTable component

1. Generate components files
$ ng generate component LeagueTable
$ ng generate component TeamData

2. Update components files (.html, .css)

## Create Class TeamData

1. Create a class to hold team data
$ ng generate class TeamData
