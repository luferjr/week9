import { Component } from '@angular/core';

@Component({
  selector: 'app-league-table',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'LeagueTable';
}
