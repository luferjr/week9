import { Component, OnInit } from '@angular/core';

import {TeamData} from '.';

@Component({
  selector: 'app-team-data',
  templateUrl: './team-data.component.html',
  styleUrls: ['./team-data.component.css']
})
export class TeamDataComponent implements OnInit {

  constructor() { }

  teams = [new TeamData(1, 'Totenham'), new TeamData(2, 'Liverpool'), new TeamData(3, 'Aston Villa')];

  ngOnInit() {
  }

}
