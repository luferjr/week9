import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LeagueTableComponent } from './league-table/league-table.component';
import { TeamDataComponent } from './team-data/team-data.component';

@NgModule({
  declarations: [
    AppComponent,
    LeagueTableComponent,
    TeamDataComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [LeagueTableComponent, TeamDataComponent]
})
export class AppModule { }
